import sys
import os
import struct
import xml.dom.minidom
import zipfile
from StringIO import StringIO

def CompileBuild(name):
    buildFile = open("build.xml", "w")

    if buildFile:
        buildFile.write("<Build name=\"" + name + "\">\r")
        bildFile = open("prebild.xml", "r")

        if bildFile:
            data = bildFile.read()
            buildFile.write(data)
            bildFile.close()

        buildFile.write("</Build>\r")

    buildFile.close()

def CompileAnimation(names):
    animationFile = open("animation.xml", "w")

    if animationFile:
        animationFile.write("<Anims>\r")

        for name in names:
            animFile = open("preanim_" + name + ".xml", "r")

            if animFile:
                data = animFile.read()
                animationFile.write(data)
                animFile.close()

        animationFile.write("</Anims>\r")
    animationFile.close()

def GetImages():
    result = []
    imagesFile = open("images.lst", "r")

    if imagesFile:
        while True:
            line = imagesFile.readline()
            if not line:
                break
            else:
                line = line.replace("\r", "")
                line = line.replace("\n", "")
                if line != "":
                    result.append(line)

        imagesFile.close()

    return result

def Execute():
    CompileBuild("nigthmarephaseindicator")
    CompileAnimation(["calm_pre", "calm_loop", "calm_pst", "warn_pre", "warn_loop", "warn_pst", "wild_pre", "wild_loop", "wild_pst", "dawn_pre", "dawn_loop", "dawn_pst", "wild_lock", "wild_staticlock"])

    zf = zipfile.ZipFile("nigthmarephaseindicator.zip", "w", zipfile.ZIP_DEFLATED)
    zf.write("build.xml")
    zf.write("animation.xml")
    names = GetImages()
    for image in names:
        zf.write(image)
    zf.close()

def main():    
    Execute()

if __name__ == '__main__':
    main()
