# Desctiption
Nightmare phase indicator - Don't Starve Together modification.

Client-side mod. Indicator shows current nightmare phase and time left. Status Announcements mod supported.

In settings, you can set:
1) Position indicator on the screen.
2) Position timer relative to the indicator.
3) Position phase name relative to the indicator.
4) Scale indicator.
5) Language for phases name.
6) Visibility timer, phase name.
7) Visibility indicator: Always visible / Visible when there is a medallion in the inventory.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1298780476)
