-- Bulgarian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Фаза спокойно",
        warn = "Фаза предупреждава",
        wild = "Фаза дива",
        dawn = "Фаза на зазоряване",
        lock = "Фаза дива заключена",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, ще завърши в {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
