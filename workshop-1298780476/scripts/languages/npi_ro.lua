-- Romanian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Faza calmă",
        warn = "Faza avertizează",
        wild = "Faza sălbatică",
        dawn = "Faza zorilor",
        lock = "Faza sălbatică blocată",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, se va termina în {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
