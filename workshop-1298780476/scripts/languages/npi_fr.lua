-- French
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Phase calme",
        warn = "Phase prévenir",
        wild = "Phase sauvage",
        dawn = "Phase aube",
        lock = "Phase sauvage verrouillé",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, se terminera par {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
