-- Belorussian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Фаза спакойная",
        warn = "Фаза папярэдзiць",
        wild = "Фаза дзiкае",
        dawn = "Фаза свiтання",
        lock = "Фаза дзiкае зачынены",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, скончыцца праз {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
