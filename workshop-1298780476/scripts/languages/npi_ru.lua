-- Russian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Фаза спокойствия",
        warn = "Фаза предупреждения",
        wild = "Фаза кошмара",
        dawn = "Фаза окончания",
        lock = "Фаза кошмара заблокирована",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, закончится через {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
