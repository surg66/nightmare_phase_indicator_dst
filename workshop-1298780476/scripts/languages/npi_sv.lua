-- Swedish
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fas lugn",
        warn = "Fas varning",
        wild = "Fas vild",
        dawn = "Fas gryning",
        lock = "Fas vild låst",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, kommer att sluta om {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
