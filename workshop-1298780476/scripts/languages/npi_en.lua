-- English
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Phase calm",
        warn = "Phase warn",
        wild = "Phase wild",
        dawn = "Phase dawn",
        lock = "Phase wild locked",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, will end in {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
