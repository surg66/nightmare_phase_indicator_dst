-- Italian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fase calma",
        warn = "Fase di avvertimento",
        wild = "Fase selvaggia",
        dawn = "Fase alba",
        lock = "Fase selvaggia bloccata",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, finirà in {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
