-- Spanish
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fase de calma",
        warn = "Advertencia de fase",
        wild = "Fase salvaje",
        dawn = "Fase de amanecer",
        lock = "Fase salvaje bloqueada",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, terminará en {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
