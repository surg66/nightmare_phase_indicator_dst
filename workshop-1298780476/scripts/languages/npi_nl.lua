-- Nederlands (Dutch)
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fase kalm",
        warn = "Fase waarschuwen",
        wild = "Fase wild",
        dawn = "Fase dawn",
        lock = "Fase wild opgesloten",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, zal eindigen in {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
