-- Finnish
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Vaihe rauhallinen",
        warn = "Vaihe varoittaa",
        wild = "Vaihe villi",
        dawn = "Vaihe aamunkoitto",
        lock = "Vaihe villi lukittu",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, päättyy {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
