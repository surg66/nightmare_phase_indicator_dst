-- Turkish
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Aşama sakin",
        warn = "Aşama uyarı",
        wild = "Aşama vahşi",
        dawn = "Aşama şafak",
        lock = "Aşama vahşi kilitli",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, bitecek {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
