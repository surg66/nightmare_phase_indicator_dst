-- Traditional Chinese
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "平靜階段",
        warn = "警告階段",
        wild = "夢魘階段",
        dawn = "黎明階段",
        lock = "夢魘階段鎖定",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}將以 {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
