name = "Nightmare phase indicator"
description = [[Indicator shows current nightmare phase and time left.
Status Announcements mod supported.

In settings, you can set:
1) Position indicator on the screen.
2) Position timer relative to the indicator.
3) Position phase name relative to the indicator.
4) Scale indicator.
5) Language for phases name.
6) Visibility timer, phase name.
7) Visibility indicator: Always visible / Visible when there is a medallion in the inventory.
]]
author = "surg"
version = "2.0.5"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
hamlet_compatible = false
dst_compatible = true
all_clients_require_mod = false
client_only_mod = true
server_filter_tags = {""}

local horizontal_margin_options = {}
local vertical_margin_options = {}

-- 64 - RESOLUTION_X/2 = 640
for i = 1, 64 do
    local value = (i - 1) * 10
    horizontal_margin_options[i] = { description = ""..value, data = value }
end

-- 36 - RESOLUTION_Y/2 = 360
for i = 1, 36 do
    local value = (i - 1) * 10
    vertical_margin_options[i] = { description = ""..value, data = value }
end

configuration_options =
{
    {
        name  = "LANGUAGE",
        label = "Language",
        hover = "Sets language",
        options =   {
                        {description = "Белоруский",  hover = "Belorussian",         data = "npi_be"},
                        {description = "Български",   hover = "Bulgarian",           data = "npi_bg"},
                        {description = "简体中文",      hover = "Chinese Simplified",  data = "npi_chs"},
                        {description = "繁體中文",      hover = "Chinese Traditional", data = "npi_cht"},
                        {description = "čeština",     hover = "Czech",               data = "npi_cs"},
                        {description = "Nederlands",  hover = "Dutch",               data = "npi_nl"},
                        {description = "English",     hover = "English",             data = "npi_en"},
                        {description = "Français",    hover = "French",              data = "npi_fr"},
                        {description = "Suomi",       hover = "Finnish",             data = "npi_fi"},
                        {description = "Deutsch",     hover = "German",              data = "npi_de"},
                        {description = "Italiano",    hover = "Italian",             data = "npi_it"},
                        {description = "日本語",       hover = "Japanese",            data = "npi_ja"},
                        {description = "한국어",        hover = "Korean",              data = "npi_ko"},
                        {description = "Polski",      hover = "Polish",              data = "npi_pl"},
                        {description = "Português",   hover = "Portuguese",          data = "npi_pt"},
                        {description = "Română",      hover = "Romanian",            data = "npi_ro"},
                        {description = "Pусский",     hover = "Russian",             data = "npi_ru"},
                        {description = "Español",     hover = "Spanish",             data = "npi_es"},
                        {description = "Svenska",     hover = "Swedish",             data = "npi_sv"},
                        {description = "Türkçe",      hover = "Turkish",             data = "npi_tr"},
                        {description = "Украiнський", hover = "Ukrainian",           data = "npi_uk"}
                    },
        default = "npi_en",
    },
    {
        name    = "VISIBLY_HAVE_MEDALLION",
        label   = "Visibility",
        hover   = "Sets visibility",
        options =   {
                        { description = "Have medallion", data = true },
                        { description = "Always",         data = false }
                    },
        default = false
    },
    {
        name    = "VISIBLE_ANIMATION",
        label   = "Animation",
        hover   = "Sets animation",
        options =   {
                        { description = "On",  data = true  },
                        { description = "Off", data = false }
                    },
        default = true
    },
    {
        name    = "TIMER",
        label   = "Timer",
        hover   = "Sets timer visibility and position",
        options =   {
                        { description = "Hide",   data = "NONE"   },
                        { description = "Top",    data = "TOP"    },
                        { description = "Bottom", data = "BOTTOM" },
                        { description = "Center", data = "CENTER" },
                        { description = "Left",   data = "LEFT"   },
                        { description = "Right",  data = "RIGHT"  }
                    },
        default = "BOTTOM"
    },
    {
        name    = "PHASE_NAME",
        label   = "Phase name",
        hover   = "Sets phase name visibility and position",
        options =   {
                        { description = "Hide",   data = "NONE"   },
                        { description = "Top",    data = "TOP"    },
                        { description = "Bottom", data = "BOTTOM" },
                        { description = "Center", data = "CENTER" },
                        { description = "Left",   data = "LEFT"   },
                        { description = "Right",  data = "RIGHT"  }
                    },
        default = "CENTER"
    },
    {
        name    = "HORIZONTAL_ALIGNMENT",
        label   = "Horizontal alignment",
        hover   = "Sets horizontal alignment",
        options =   {
                        { description = "Left",   data = "LEFT"   },
                        { description = "Center", data = "CENTER" },
                        { description = "Right",  data = "RIGHT"  }
                    },
        default = "CENTER"
    },
    {
        name    = "VERTICAL_ALIGNMENT",
        label   = "Vertical alignment",
        hover   = "Sets vertical alignment",
        options =   {
                        { description = "Top",    data = "TOP"    },
                        { description = "Center", data = "CENTER" },
                        { description = "Bottom", data = "BOTTOM" }
                    },
        default = "TOP"
    },
    {
        name    = "HORIZONTAL_MARGIN",
        label   = "Horizontal margin",
        hover   = "Sets horizontal margin. If horizontal alignment is CENTER then horizontal margin not work.",
        options = horizontal_margin_options,
        default = 0
    },
    {
        name    = "VERTICAL_MARGIN",
        label   = "Vertical margin",
        hover   = "Sets vertical margin. If vertical alignment is CENTER then vertical margin not work.",
        options = vertical_margin_options,
        default = 50
    },
    {
        name    = "SCALE",
        label   = "Scale",
        hover   = "Sets scale",
        options =   {
                        { description = "100%", data = 1   },
                        { description = "90%",  data = 0.9 },
                        { description = "80%",  data = 0.8 },
                        { description = "70%",  data = 0.7 },
                        { description = "60%",  data = 0.6 },
                        { description = "50%",  data = 0.5 },
                        { description = "40%",  data = 0.4 },
                        { description = "30%",  data = 0.3 },
                    },
        default = 1
    },
}
